/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Efren Rogelio Narvaez Peña (efren092@gmail.com)
 * @date 10-mar-2017
 * @time 22:58:42
 */
public class FlatternerDos {

    public static List<Object> Descomponer(List<Object> lista) {
        List<Object> resultado = new ArrayList<>();
        LinkedList<Object> auxiliar = new LinkedList<>(lista);
        while (auxiliar.size() > 0) {
            Object primerElemento = auxiliar.remove();
            if (primerElemento instanceof List<?>) {
                auxiliar.addAll(0, (List<?>) primerElemento);
              
            } else {
                resultado.add(primerElemento);
            }

        }
         resultado.removeAll(Collections.singleton(null));  
        return resultado;
    }

}
