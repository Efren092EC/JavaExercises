package app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Flattener {

    /**
     * @param args the command line arguments
     */
    public static List<Object> flatten(List<Object> list) {
        List<Object> result = new ArrayList<>();
        LinkedList<Object> que = new LinkedList<>(list);
        while (que.size() > 0) {
            Object e = que.remove();
            if (e instanceof List<?>) {      
                que.addAll(0, (List<?>) e);
            } else {
                result.add(e);
            }
        }
        result.removeAll(Collections.singleton(null));      
        return result;
    }
}
