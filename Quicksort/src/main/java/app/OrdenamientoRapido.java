/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

/**
 *
 * @author Efren Rogelio Narvaez Peña (efren092@gmail.com)
 * @date 12-mar-2017
 * @time 10:48:06
 */
public class OrdenamientoRapido {

    public static int[] QuickShort(int[] lista, int izq, int dere) {

        int pivot = lista[izq];
        int i = izq;
        int d = dere;
        int aux;
        while (i < d) {
            while (lista[i] <= pivot && i < d) {
                i++;

            }
            while (lista[d] > pivot) {
                d--;

            }
            if (i < d) {
                aux = lista[i];
                lista[i] = lista[d];
                lista[d] = aux;
            }
        }
        lista[izq] = lista[d];
        lista[d] = pivot;
        if (izq < d - 1) {
            QuickShort(lista, izq, d - 1);
        }
        if (d + 1 > dere) {
            QuickShort(lista, d + 1, dere);
        }

        return lista;
    }
}
