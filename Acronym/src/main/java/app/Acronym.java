package app;

public class Acronym {

    public static String generate(String frase) {

        String asincro = "";

        for (int i = 0; i < frase.length(); i++) {
            if (i == 0) {
                asincro = String.valueOf(frase.charAt(i));
            } else if (frase.charAt(i) == ' ' || frase.charAt(i) == '-') {
                asincro = asincro + String.valueOf(frase.charAt(i + 1));
            } else if (frase.charAt(i - 1) != ' ' && Character.isUpperCase(frase.charAt(i)) && i != 1 && i != 2) {
                asincro = asincro + String.valueOf(frase.charAt(i));
            }
        }

        asincro = asincro.toUpperCase();
        System.out.println(asincro);
        return asincro;
    }
}
