package app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class PrimeFactors {

 

    public static List<Long> getForNumber(long number) {
        List<Long> fatoresprios = new ArrayList<>();
            
        Long i = 2L;
        while (number / i != 0) {
            if (number % i == 0) {
                fatoresprios.add(i);                
                number = number / i;
            } else {
                i++;
            }

        }

      
        return fatoresprios;
    }
}
