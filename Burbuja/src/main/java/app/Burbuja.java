package app;

public class Burbuja {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] numeros = {5, 6, 1, 0, 3};
        int[] resultado = Listado(numeros);
        for (int i = 0; i < resultado.length; i++) {
            int numero = resultado[i];
            System.out.println(numero);

        }
    }

    public static int[] Listado(int[] input) {
        int aux;
        for (int i = 1; i < input.length; i++) {
            for (int j = 0; j < input.length-1; j++) {
                if (input[j] > input[j + 1]) {
                    aux = input[j];
                    input[j] = input[j+1];
                    input[j+1] = aux;
                }

            }

        }
        return input;
    }
}
