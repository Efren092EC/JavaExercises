package app;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class WordCount {

    public static Map<String, Integer> phrase(String frase) {
        frase = frase.toLowerCase();
        frase = frase.replaceAll("[^a-z0-9]", " ");
        List<String> tokens = new ArrayList<>();
        
        Map<String, Integer> map = new HashMap<String, Integer>();
        
        StringTokenizer frasetokens = new StringTokenizer(frase);
        while (frasetokens.hasMoreElements()) {
            Object nextElement = frasetokens.nextElement();
            tokens.add(String.valueOf(nextElement));

        }
        for (String temp : tokens) {
            Integer count = map.get(temp);
            map.put(temp, (count == null) ? 1 : count + 1);
        }

        return map;
    }
}
