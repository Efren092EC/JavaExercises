package app;

import java.util.HashSet;
import java.util.Set;

public class PangramChecker {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

    }

    public static boolean isPangram(String sentence) {
        sentence = sentence.toUpperCase();
        sentence = sentence.replaceAll("[^A-Z]", "");

        Set<Character> set = new HashSet<Character>();
        for (int i = 0; i < sentence.length(); i++) {
            set.add(sentence.charAt(i));

        }
        return set.size() == 26 ? true : false;

    }
}
