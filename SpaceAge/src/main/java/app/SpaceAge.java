package app;

public class SpaceAge {

    private static long seconds;

    SpaceAge(long i) {
        seconds = i;
    }

    public static double getSeconds() {
        return seconds;
    }

    public static double onEarth() {

        double age = getSeconds() / 31557600;
        return age;

    }

    public static double onMercury() {
        double age = getSeconds() /7600543.81992;
        return age;
    }

    public static double onVenus() {
        double age = getSeconds() / 19414149.0522;
        return age;
    }

    public static double onMars() {
        double marte =  1.8808158*31557600;
        double age = getSeconds() /marte;      
        System.out.println(age);
        return age;
    }

    public static double onJupiter() {
         double jupiter =  11.862615*31557600;
        double age = getSeconds() / jupiter;
        return age;
    }

    public static double onSaturn() {
        double saturn =  29.447498*31557600;
        double age = getSeconds() / saturn;
        return age;
    }

    public static double onUranus() {
        double uranus =  84.016846*31557600;
        double age = getSeconds() / uranus;
        return age;
    }

    public static double onNeptune() {
         double neptune =  164.79132*31557600;
        double age = getSeconds() / neptune;
        return age;
    }
    /**
     * @param args the command line arguments
     */

}
