package app;

public class Hamming {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        System.out.println(compute("AT", "CT"));
    }

    public static int compute(String adn, String diference) {
        int cont = 0;
        if (adn.length() == diference.length()) {
            for (int i = 0; i < adn.length(); i++) {
                if (adn.charAt(i) != diference.charAt(i)) {
                    cont++;
                }

            }
        } else {
            throw new IllegalArgumentException();
        }

        return cont;
    }
}
