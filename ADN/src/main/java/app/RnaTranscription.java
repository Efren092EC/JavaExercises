package app;

import java.util.Locale;

public class RnaTranscription {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    }

    public static String ofDna(String rna) {
        if (rna.length() > 1) {
            String adntorna = "";
            String aux = "";

            for (int i = 0; i < rna.length(); i++) {
                aux = cambio(String.valueOf(rna.charAt(i)));
                adntorna = adntorna + aux;
            }
            return adntorna;
        } else {
            rna = cambio(rna);
        }

        return rna;
    }

    public static String cambio(String rna) {
        
      
        switch (rna) {
            case "G":
                rna = "C";
                break;
            case "C":
                rna = "G";
                break;
            case "T":
                rna = "A";
                break;
            case "A":
                rna = "U";
                break;
            case "":
                rna = "";
        }
        return rna;
    }
}
